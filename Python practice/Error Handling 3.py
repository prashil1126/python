def ask():
    while True:
        try:
            number = int(input("Please enter a number: "))
        except:
            print("An error occurred! Please try again!")
            continue
        else:
            result = number**2
            print("The number squares is: {} ".format(result))
            break
        finally:
            print("Handled")


ask()
