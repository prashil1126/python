from random import randint

def dice_roller():
    return randint(1, 6)


def turn_checker(turn_value, User1, User2, User3, User4):
    steps_to_goal = [56, 56, 56, 56]
    while True:
        if turn_value == 0:
            red_roll = dice_roller()
            steps_to_goal[0] -= red_roll
            turn_value = check_if_goal(red_roll, steps_to_goal[0], User1, turn_value)

        elif turn_value == 1:
            yellow_roll = dice_roller()
            steps_to_goal[1] -= yellow_roll
            turn_value = check_if_goal(yellow_roll, steps_to_goal[1], User2, turn_value)

        elif turn_value == 2:
            green_roll = dice_roller()
            steps_to_goal[2] -= green_roll
            turn_value = check_if_goal(green_roll, steps_to_goal[2], User3, turn_value)

        elif turn_value == 3:
            blue_roll = dice_roller()
            steps_to_goal[3] -= blue_roll
            turn_value = check_if_goal(blue_roll, steps_to_goal[3], User4, turn_value)


def check_if_goal(rolled_number, steps_left, user, turn):
    print(user + " Rolled " + str(rolled_number))
    if steps_left > 0:
        print(user + " has " + str(steps_left) + " steps left to reach the goal!")
    elif steps_left == 0:
        print(user + " has reached the goal")
        exit(0)
    elif rolled_number > steps_left:
        print(" Oops, wait next turn")

    if turn != 3 and rolled_number != 6:
        return turn + 1
    elif turn == 3 and rolled_number != 6:
        return turn - 3
    elif rolled_number == 6:
        return turn


# Driver Code
Red = input("Enter Red Player's Name ")
Yellow = input("Enter Yellow Player's Name ")
Green = input("Enter Green Player's Name ")
Blue = input("Enter Blue Player's Name ")
turn = 0
turn_checker(turn, Red, Yellow, Green, Blue)
