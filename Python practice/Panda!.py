import numpy as nm
import pandas as pd


df = pd.DataFrame(
    [['Prashil', 40, 35, 44, 50, 72],
     ["Rajeev", 24, 24, 22, 50, 11]],
    index=[0, 1],
    columns=["name", "Software Engineering", "Compiler", "E-commerce", "WebTech", "RTS"]
)

print(df)