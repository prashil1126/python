from random import *


# Generate cards(which are actually numbers)
def cards():
    """
    Use randint to generate numbers between 1 and 13, corresponding to cards Ace through King.
    """
    list1 = []
    for i in range(0, 3):
        rand = randint(1, 13)
        list1.append(rand)
        list1.sort()
    return list1


# Check win by checking the highest trial, 1st in hierarchical order
def win_check_by_trial(list1, list2, user1, user2):
    trial1 = False
    trial2 = False
    if list1[0] == list1[1] == list1[2]:
        trial1 = True
        card1 = list1[0]
        if card1 == 1:
            card1 = 100

    if list2[0] == list2[1] == list2[2]:
        trial2 = True
        card2 = list2[0]
        if card2 == 1:
            card2 = 100

    if trial1:
        if not trial2:
            print(user1 + " Wins by highest trial!")
        else:
            if card1 > card2:
                print(user1 + " Wins by highest trial!")
            else:
                print(user2 + " Wins by highest trial!")

    elif trial2:
        print(user2 + " Wins by highest trial!")

    if not trial1 and not trial2:
        win_check_by_run(list1, list2, user1, user2)


# Check win by checking the highest run, 2nd in hierarchical order
def win_check_by_run(list1, list2, user1, user2):
    run1 = False
    run2 = False
    if list1[0] + 1 == list1[1] and list1[0]+2 == list1[2]:
        run1 = True
    if list2[0] + 1 == list2[1] and list2[0]+2 == list2[2]:
        run2 = True

    if run1:
        if not run2:
            print(user1 + " Wins by highest run!")
        else:
            card1 = list1[2]
            card2 = list2[2]
            if 1 in list1:
                card1 = 100
            if 1 in list2:
                card2 = 100
            if card1 > card2:
                print(user1 + " Wins by highest run!")
            elif card2 > card1:
                print(user2 + " Wins by highest run!")

    elif run2:
        print(user2 + " Wins by highest run!")

    if not run1 and not run2:
        win_check_by_joot(list1, list2, user1, user2)


# Check win by checking the highest joot, 3rd in hierarchical order
def win_check_by_joot(list1, list2, user1, user2):
    joot1 = False
    joot2 = False
    set1 = set
    set2 = set
    for item in list1:
        if list1.count(item) == 2:
            joot1 = True
            set1 = {item}

    for item in list2:
        if list2.count(item) == 2:
            joot2 = True
            set2 = {item}

    if joot1:
        if not joot2:
            print(user1 + " Wins by highest joot!")
        else:
            if 1 in set1:
                set1 = {100}
            if 1 in set2:
                set2 = {100}
            for card1 in set1:
                for card2 in set2:
                    if card1 > card2:
                        print(user1 + " Wins by highest joot!")
                    elif card2 > card1:
                        print(user2 + " Wins by highest joot!")
                    else:
                        win_check_by_highest_card(list1, list2, user1, user2)

    elif joot2:
        print(user2 + " Wins by highest joot!")

    if not joot1 and not joot2:
        win_check_by_highest_card(list1, list2, user1, user2)


# Check win by checking highest card, 4th in hierarchical order
def win_check_by_highest_card(list1, list2, user1, user2):
    """
    Check which of the list has highest card if one doesn't beat the other by
    trial,run or joot!
    """
    while True:
        try:
            largest1 = max(list1)
            largest2 = max(list2)
        except:
            print("Game Drawn!")
            break
        else:
            if 1 in list1:
                largest1 = 100
            if 1 in list2:
                largest2 = 100
            if largest2 > largest1:
                print(user2 + " Wins by highest card!")
                break
            elif largest1 > largest2:
                print(user1 + " Wins by highest card!")
                break
            elif largest2 == largest1:
                list1.remove(largest1)
                list2.remove(largest2)
                continue


# Return the list with A,J,Q,K.
def jokers(no_joker):
    """
    Check if the list has 1,11,12 or 13 to return face cards or ace.
    """
    joker_list = []
    for item in no_joker:
        if item == 1:
            joker_list.append("A")
        elif item == 11:
            joker_list.append("J")
        elif item == 12:
            joker_list.append("Q")
        elif item == 13:
            joker_list.append("K")
        else:
            joker_list.append(item)
    return joker_list


# Driver Code:
User1 = input("Enter user1 ko name ")
User2 = input("Enter user2 ko name ")

card_set_1 = cards()
card_set_2 = cards()

print(User1 + "=")
print(jokers(card_set_1))

print(User2 + "=")
print(jokers(card_set_2))

win_check_by_trial(card_set_1, card_set_2, User1, User2)
