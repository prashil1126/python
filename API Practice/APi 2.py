from flask import Flask, jsonify, request


app = Flask(__name__)
students = []


# View all students
@app.route("/students")
def get_students():
    return jsonify({"Students": students})


# View specific student
@app.route("/students/<string:name>")
def get_specific_student(name):
    for student in students:
        if student["name"] == name:
            return jsonify(student)

    return jsonify({"message": "not found, sorry!"})


# Add a student
@app.route('/students', methods=['POST'])
def create_student():
    request_data = request.get_json()
    new_student = {
        'name': request_data["name"],
        "marks": request_data["marks"]
    }
    students.append(new_student)
    return jsonify(new_student)


app.run(port=2500)
