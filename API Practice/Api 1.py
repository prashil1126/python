from flask import Flask, jsonify, request, render_template


app = Flask(__name__)
stores = [
    {
        "name": "My Store",
        "items": [
            {
                "name": "My Item",
                "price": "15000"
            }
        ]
    }
]

@app.route("/")
def home():
    return render_template("index.html")


# POST /store data:{"name"}: Create a store with the given name
@app.route('/store', methods=['POST'])
def create_store():
    # global stores
    request_data = request.get_json()
    new_store = {
        'name': request_data["name"],
        "items": []
    }
    stores.append(new_store)
    print(stores)
    return jsonify(new_store)


# GET /store/<string:name> Retrieve the store with given name
@app.route('/store/<string:name>')
def get_store(name):
    # Iterate over stores
    print(stores)
    for store in stores:
        if store["name"] == name:
            return jsonify(store)

    return jsonify({"message": "Store not found"})


# GET /store Retrieve the list of all stores
@app.route("/store")
def get_stores():
    return jsonify({'stores': stores})


"""POST /store/<string:name>/item {"name":, "price":} 
 Add an item in the store of given name with associated name and price of the item"""
@app.route("/store/<string:name>/item", methods=['POST'])
def create_item_in_store(name):
    request_data = request.get_json()
    for store in stores:
        if store["name"] == name:
            new_item = {
                "name": request_data["name"],
                "price": request_data["price"]
            }
            store["items"].append(new_item)
            return jsonify(new_item)
    return jsonify({"message": "Store not found"})

# GET /store/<string:name>/item Retrieve the list of items from a store with a specific name
@app.route("/store/<string:name>/item")
def get_items_in_store(name):
    for store in stores:
        if store["name"] == name:
            return jsonify({"items": store["items"]})
        else:
            return jsonify({"message": "Item not found"})



app.run(port=5000)
